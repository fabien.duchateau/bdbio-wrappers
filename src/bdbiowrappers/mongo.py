#!/usr/bin/env python3
# encoding: utf-8
# =============================================================================
# Class for interacting with MongoDB
# =============================================================================

import pymongo


class WrapperMongo:
    """
    WrapperMongo class for querying MongoDB databases.

    Uses the standard json library:
    http://api.mongodb.com/python/current/
    """
    
    def __init__(self, host, username, password, db, collection):
        self.host = host
        self.username = username
        self.password = password
        self.db_name = db  # name of the database
        self.connection_status = True  # by default, connection is ok
        self.connection = None
        try:
            self.connection = pymongo.MongoClient(host=host, username=username, password=password, authSource=db,
                                                  serverSelectionTimeoutMS=10)  # default localhost:27017
            self.connection.server_info()  # forces a query to MongoDB (for checking the connection)
        except pymongo.errors.ServerSelectionTimeoutError as e:
            print('Could not connect to the MongoDB database ! Is the MongoDB server running ? \n' + str(e))
            print('Remind that the MongoDB instance on bd-pedago server requires a VPN connection outside of the campus.')
            self.connection_status = False
        self.database = self.connection[self.db_name]
        self.collection = self.database[collection]

    def insert(self, documents):
        # documents = list of dict, eg [ {'a': 1, 'b': 'hello'}, {'c': 1}]
        return self.collection.insert_many(documents)

    def change_collection(self, new_db, new_collection):
        self.db_name = new_db 
        self.database = self.connection[self.db_name]
        self.collection = self.database[new_collection]

    def drop_collection(self):
        # deletes the current collection
        return self.collection.drop()

    def count(self, filter={}):
        # count the number of documents for the given filter
        return self.collection.count_documents(filter)

    def query(self, filter=None, projected_fields=None):
        return self.collection.find(filter, projected_fields)

    def get_one_document(self):
        return self.collection.find_one()

    def aggregate(self, pipeline):
        # pipeline = list of aggregation steps: https://pymongo.readthedocs.io/en/stable/examples/aggregation.html
        return self.collection.aggregate(pipeline)
