#!/usr/bin/env python3
# encoding: utf-8
# =============================================================================
# Class for interacting with JSON files
# =============================================================================

import json


class WrapperJSON:
    """
    WrapperJSON class for parsing json file.

    Uses the standard json library:
    https://docs.python.org/library/json.html
    """
    
    def __init__(self, json_file):
        self.json_file = json_file  # path of the json file

    def parse_to_dict(self):
        with open(self.json_file, encoding='utf-8') as data_file:
            data = json.loads(data_file.read())
            return data
