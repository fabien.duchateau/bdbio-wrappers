#!/usr/bin/env python
# encoding: utf-8
# =============================================================================
# Class for interacting with SQL databases stored in SQLite
# =============================================================================

import logging
import sqlite3

logging.basicConfig(format='[%(levelname)s] - %(name)s - %(asctime)s : %(message)s')
logger = logging.getLogger("sqlite")
logger.setLevel(logging.DEBUG)


class WrapperSQLite:
    """
    WrapperSQLite class for connecting and querying a SQLite database.

    Uses the standard SQLite library:
    https://docs.python.org/library/sqlite3.html
    """

    def __init__(self, db_name):
        self.conn = None  # connection link to a database to None
        self.db_name = db_name  # name of the database
        self.connect_db()  # connect to db

    # connect to a database
    def connect_db(self):
        self.conn = sqlite3.connect(self.db_name)

    # executes a SQL query
    def query(self, query, params=()):
        try:
            cursor = self.conn.cursor()  # a cursor is used to interact with the database
            cursor.execute(query, params)
            if query.lower().startswith("select"):  # SELECT query, returns the results as a list of rows
                return cursor.fetchall()
            else:  # INSERT / DELETE / UPDATE query, returns the number of affected rows
                self.conn.commit()
                return cursor.rowcount
        except sqlite3.ProgrammingError as e:
            logger.exception(e)

    # execute a SQL script stored as a string, returns the number of affected rows
    def execute_script(self, sql_script):
        try:
            self.conn.executescript(sql_script)
            self.conn.commit()
            return self.conn.total_changes
        except sqlite3.ProgrammingError as e:
            logger.exception(e)

    # open and execute a SQL file
    def load_file(self, file_sql):
        fd = open(file_sql, 'r')
        sql = fd.read()
        fd.close()
        return self.execute_script(sql)
