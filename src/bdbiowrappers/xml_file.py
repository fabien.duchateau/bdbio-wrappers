#!/usr/bin/env python3
# encoding: utf-8
# =============================================================================
# Class for interacting with XML files
# =============================================================================

import xml.etree.ElementTree as eTree


class WrapperXML:
    """
    WrapperXML class for parsing and querying XML files.

    Uses standard library xml.etree:
    http://docs.python.org/library/xml.etree.elementtree.html#module-xml.etree.ElementTree
    """

    def __init__(self, xml_file):
        self.xml_file = xml_file  # path of the xml file
        self.tree = eTree.parse(self.xml_file)

    def get_root(self):
        return self.tree.getroot()

    def query(self, xpath):
        # in etree, xpath module is quite limited, and if the XML file has a namespace, queries must use it :(
        return self.tree.findall(xpath)
