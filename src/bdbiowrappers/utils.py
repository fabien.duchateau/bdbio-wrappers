#!/usr/bin/env python
# encoding: utf-8

import os
import sys
import yaml
from logzero import logger


def load_config_file(filepath='config.yaml'):
    # load a configuration file (authentification to different database servers), by default config.yaml
    if os.path.isfile(filepath):  # do not forget to modify the file config.yaml !
        config = yaml.safe_load(open(filepath))
    elif os.path.isfile('config-ens.yaml'):  # config enseignant
        config = yaml.safe_load(open('config-ens.yaml'))
    else:
        logger.error(f"Fichier de configuration {filepath} non trouvé ! ")
        sys.exit(1)
    return config
