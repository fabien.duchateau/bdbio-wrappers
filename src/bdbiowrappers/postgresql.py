#!/usr/bin/env python
# encoding: utf-8
# =============================================================================
# Class for interacting with SQL databases stored in PostgreSQL
# =============================================================================

import psycopg2
import logging

logging.basicConfig(format='[%(levelname)s] - %(name)s - %(asctime)s : %(message)s')
logger = logging.getLogger("postgresql")
logger.setLevel(logging.DEBUG)


class WrapperPostgres:
    """
    WrapperPostgres class for connecting and querying a PostgreSQL database.

    Uses the psycopg library:
    https://www.psycopg.org/docs/
    """

    def __init__(self, host, username, password, db):
        self.host = host
        self.username = username
        self.password = password
        self.db = db  # name of the database
        self.conn = psycopg2.connect(host=host, user=username, password=password, dbname=db)

    # executes a SQL query
    def query(self, query, params=()):
        try:
            cursor = self.conn.cursor()  # a cursor is used to interact with the database
            cursor.execute(query, params)
            if query.lower().startswith("select"):  # SELECT query, returns the results as a list of rows
                return cursor.fetchall()
            else:  # INSERT / DELETE / UPDATE query, returns the number of affected rows
                self.conn.commit()
                return cursor.rowcount
        except psycopg2.Error as e:
            logger.exception(e)

    # execute a SQL script stored as a string, returns the number of affected rows
    def execute_script(self, sql_script):
        try:
            cursor = self.conn.cursor()  # a cursor is used to interact with the database
            lines = sql_script.splitlines()
            for line in lines:
                cursor.execute(line)
            self.conn.commit()
            return len(lines)  # number of lines != number of successfully executed instructions
        except psycopg2.Error as e:
            logger.exception(e)

    # open and execute a SQL file
    def load_file(self, file_sql):
        fd = open(file_sql, 'r')
        sql = fd.read()
        fd.close()
        return self.execute_script(sql)

