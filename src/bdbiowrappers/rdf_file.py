#!/usr/bin/env python3
# encoding: utf-8
# =============================================================================
# Class for interacting with RDF files
# =============================================================================

from rdflib import Graph
import pprint


class WrapperRDF:
    """
    WrapperRDF class for parsing and querying RDF data.

    Uses the rdflib library:
    https://rdflib.readthedocs.io/en/latest/
    """

    def __init__(self, file_rdf, format_rdf):
        self.triples = Graph()
        self.triples.parse(file_rdf, format=format_rdf)

    # counts the number of triples in the graph
    def count(self):
        if self.triples:
            return len(self.triples)
        return 0

    # executes a SPARQL query
    def query(self, query):
        res = self.triples.query(query)
        return res

    # prints the graph in the console
    def print_graph(self):
        if self.triples:
            for stmt in self.triples:
                pprint.pprint(stmt)
