#!/usr/bin/env python
# encoding: utf-8

import unittest
import os
from bdbiowrappers.blazegraph import WrapperBlazegraph
from bdbiowrappers.utils import load_config_file

config = load_config_file()


class TestCase(unittest.TestCase):
    
    def setUp(self):
        current_dir = os.path.dirname(os.path.realpath(__file__))
        self.wrapper = WrapperBlazegraph(server=config['blazegraph_host'], namespace=config['blazegraph_namespace'])
        # deletion and insertion of documents to have a stable state in the DB for the tests
        res = self.wrapper.update('''
                DELETE DATA { <http://example/s1> <http://example/p1> <http://example/o1> . 
                              <http://example/s1> <http://example/p2> "objet2" .
                              <http://example/s2> <http://example/p1> <http://example/o3> .
                }
        ''')
        self.file_ttl = os.path.join(current_dir, 'data', 'bd.ttl')
        res = self.wrapper.load_file(self.file_ttl)

    def test_count(self):
        res = self.wrapper.query('select (count(*) as ?nb) where { ?s ?p ?o }')
        print(res)  # do not hesitate to print the result to check the structure
        nb = int(res['results']['bindings'][0]['nb']['value'])  # nb corresponds to the SPARQL variable ?nb
        assert (nb == 3), f"The number of triples should be 3, but current output is {nb}."

    def test_query(self):
        res = self.wrapper.query("""SELECT ?p ?o WHERE
                                    { <http://example/s1> ?p ?o . }
                                 """)
        for r in res['results']['bindings']:  # tableau des résultats
            print(r['p']['value'])
        nb = len(res['results']['bindings'])
        assert (nb == 2), f"The number of results should be 2, but current output is {nb}."


if __name__ == "__main__":
    unittest.main(verbosity=2)  # Run all tests with verbose mode
