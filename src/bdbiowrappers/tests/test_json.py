#!/usr/bin/env python
# encoding: utf-8

import unittest
import os
from bdbiowrappers.json_file import WrapperJSON


class TestCase(unittest.TestCase):

    def setUp(self):
        current_dir = os.path.dirname(os.path.realpath(__file__))
        file_json = os.path.join(current_dir, 'data', 'documents.json')
        self.wrapper = WrapperJSON(file_json)

    def test_parse_json(self):
        dict_json = self.wrapper.parse_to_dict()
        # print(dict_json)
        nb = len(dict_json)
        assert(nb == 2), f"The number of elements from parsed JSON file should be 2, but current output is {nb}."
        assert(dict_json['data'] is not None), "The parsed JSON file does not contain a 'data' key."
        nb_int = len(dict_json['data'])
        assert (nb_int == 3), f"The number of interactions should be 3, but current output is {nb_int}."


if __name__ == "__main__":
    unittest.main(verbosity=2)  # Run all tests with verbose mode
