#!/usr/bin/env python
# encoding: utf-8

from bdbiowrappers.utils import load_config_file
import unittest
from bdbiowrappers.mongo import WrapperMongo

config = load_config_file()


class TestCase(unittest.TestCase):

    # initialization (run once before all tests)
    def setUp(self):
        self.wrapper = WrapperMongo(host=config['mongo_host'], username=config['mongo_user'],
                                    password=config['mongo_password'], db=config['mongo_db'],
                                    collection=config['mongo_collection'])
        self.documents = [{'a': 'hello', 'b': 1}, {'a': 'world', 'b': 2}]
        # deletion and insertion of documents to have a stable state in the DB for the tests
        self.wrapper.collection = self.wrapper.database['test']  # switch to local collection test
        self.wrapper.drop_collection()
        res = self.wrapper.insert(self.documents)
        nb = len(res.inserted_ids)
        assert(nb == 2), f"The number of inserted documents should be 2, but current output is {nb}."

    def test_count(self):
        assert self.wrapper.connection_status, "No connection to the Mongo database."
        res = self.wrapper.count()
        assert(res == 2), f"The number of documents should be 2, but current output is {res}."
        res = self.wrapper.count({'b': {'$gt': 1}})  # count documents with b greater than 1
        assert(res == 1), f"The number of selected documents should be 1, but current output is {res}."

    def test_change_collection(self):
        assert self.wrapper.connection_status, "No connection to the Mongo database."
        res = self.wrapper.count()
        self.wrapper.change_collection(config['mongo_db'], 'test2')
        self.wrapper.drop_collection()  # needed to create an empty collection
        self.wrapper.insert([{'x': 0}, {'y': 0}, {'z': 0} ])
        res = self.wrapper.count()
        assert(res == 3), f"The number of documents should be 3, but current output is {res}."

    def test_find_one(self):
        assert self.wrapper.connection_status, "No connection to the Mongo database."
        res = self.wrapper.get_one_document()
        assert(res is not None), "The method find_one() should return a document, but current output is None."
        assert(res['_id'] and res['a']), f"The document should have fields _id and a."

    def test_query(self):
        assert self.wrapper.connection_status, "No connection to the Mongo database."
        res = self.wrapper.query()  # get all documents
        nb = len(list(res))
        assert(nb == 2), f"The method query() should return 2 documents, but current output is {nb}."
        res = self.wrapper.query({'b': {'$gt': 1}}, ['a'])  # get documents with b greater than 1, with field a
        nb = len(list(res))
        assert(nb == 1), f"The method query() should return 1 document, but current output is {nb}."

    def test_aggregate(self):
        assert self.wrapper.connection_status, "No connection to the Mongo database."
        res = self.wrapper.aggregate([{"$match": {"a": "hello"}}])
        assert(res is not None), f"Aggregation should return a list of documents, but current output is None."
        nb = len(list(res))
        assert (nb == 1), f"The number of retrieved documents should be 1, but current output is {nb}."


if __name__ == '__main__':
    unittest.main(verbosity=2)  # Run all tests with verbose mode
