#!/usr/bin/env python
# encoding: utf-8

import unittest
import os
from bdbiowrappers.rdf_file import WrapperRDF


class TestCase(unittest.TestCase):
    
    def setUp(self):
        file_rdf = os.path.join('data', 'bd.ttl')
        format_file_rdf = 'n3'  # format of the RDF data from tp4
        self.wrapper = WrapperRDF(file_rdf, format_file_rdf)  # parsing file
        
    def test_count(self):
        nb_triples = self.wrapper.count()
        assert(nb_triples == 3), f"The number of triples should be 3, but current output is {nb_triples}"

    def test_query(self):
        query = """SELECT DISTINCT ?o 
                   WHERE  { <http://example/s1> ?p ?o . }
                """
        res = self.wrapper.query(query)
        nb = len(res)
        assert(nb == 2), f"The number of results should return 2 triples, but current output is {nb}"


if __name__ == "__main__":
    unittest.main(verbosity=2)  # Run all tests with verbose mode
