#!/usr/bin/env python
# encoding: utf-8

import unittest
import os
from bdbiowrappers.sqlite import WrapperSQLite


class TestCase(unittest.TestCase):

    # initialization (run once before all tests)
    def setUp(self):
        current_dir = os.path.dirname(os.path.realpath(__file__))
        self.db = os.path.join(current_dir, 'data', 'test.db')  # path to data source (SQLite DB)
        self.file_schema = os.path.join(current_dir, 'data', 'bd.sqlite')  # sql file to create the DB schema
        self.wrapper = WrapperSQLite(self.db)  # SQL wrapper to the database

    def test_load_file(self):
        self.wrapper.load_file(self.file_schema)
        nb = len(self.wrapper.query("SELECT * FROM sqlite_master WHERE type='table';"))
        assert(nb == 1), f"The script should execute 1 instruction, but current output is {nb}."

    def test_select(self):
        query = 'SELECT * FROM Genes'
        res = self.wrapper.query(query)
        nb = len(res)
        assert(nb == 8), f"The number of genes should be 8, but current output is {nb}."
        query = 'SELECT * FROM Genes WHERE gene_id > ?'
        params = (5,)
        res = self.wrapper.query(query, params)
        nb = len(res)
        assert(len(res) == 3), f"The number of genes with a gene_id>5 should be 3, but current output is {nb}."

    def test_insert(self):
        query = "INSERT INTO Genes VALUES (9, 'a', 'a', 'a', 'a', 1, 'a', NULL);"
        nb = self.wrapper.query(query)
        assert (nb == 1), f"The number of insertions should be 1, but current output is {nb}."

    def test_update(self):
        query = "UPDATE Genes SET start = 'b' WHERE gene_id = 1;"
        nb = self.wrapper.query(query)
        assert (nb == 1), f"The number of updates should be 1, but current output is {nb}."

    def test_delete(self):
        query = "DELETE FROM Genes WHERE gene_id = 2;"
        nb = self.wrapper.query(query)
        assert (nb == 1), f"The number of deletes should be 1, but current output is {nb}."


if __name__ == '__main__':
    unittest.main(verbosity=2)  # Run all tests with verbose mode
