#!/usr/bin/env python
# encoding: utf-8

import unittest
import os
from bdbiowrappers.xml_file import WrapperXML


class TestCase(unittest.TestCase):

    # initialization (run once before all tests)
    def setUp(self):
        current_dir = os.path.dirname(os.path.realpath(__file__))
        file = os.path.join(current_dir, 'data', 'bd.xml')
        self.wrapper = WrapperXML(file)

    def test_root(self):
        root_tag = self.wrapper.get_root().tag
        assert(root_tag == "{http://uniprot.org/uniprot}uniprot"), f"Root tag should be {{http://uniprot.org/uniprot}}" \
                                                                   f"uniprot, but current output is {root_tag}."

    def test_query(self):
        entries = self.wrapper.query(".//{http://uniprot.org/uniprot}entry")  # . is due to a FutureWarning
        assert(len(entries) == 2), f"The XML file should contain 2 entries, but current output is {len(entries)}"


if __name__ == "__main__":
    unittest.main(verbosity=2)  # Run all tests with verbose mode
