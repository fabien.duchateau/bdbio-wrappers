#!/usr/bin/env python
# encoding: utf-8

import unittest
from bdbiowrappers.postgresql import WrapperPostgres
import os
from bdbiowrappers.utils import load_config_file

config = load_config_file()


class TestCase(unittest.TestCase):

    # initialization (run once before all tests)
    def setUp(self):
        self.file_schema = os.path.join('data', 'bd.postgresql')
        self.wrapper = WrapperPostgres(host=config['postgres_host'], username=config['postgres_user'],
                                       password=config['postgres_password'], db=config['postgres_db'],)
        # deletion and insertion of documents to have a stable state in the DB for the tests
        self.wrapper.load_file(self.file_schema)

    def test_load_file(self):
        nb = self.wrapper.load_file(self.file_schema)
        assert(nb == 10), f"The script should execute 10 instructions, but current output is {nb}."

    def test_select(self):
        query = 'SELECT COUNT(*) FROM Genes'
        res = self.wrapper.query(query)
        nb = res[0][0]  # res is a list of tuples
        assert(nb == 8), f"The number of genes should be 8, but current output is {nb}."
        query = 'SELECT * FROM Genes WHERE gene_id > %s'
        params = (5,)
        res = self.wrapper.query(query, params)
        nb = len(res)
        assert(nb == 3), f"The number of genes with a gene_id>5 should be 3, but current output is {nb}."

    def test_insert(self):
        query = "INSERT INTO Genes VALUES (9, 'a', 'a', 'a', 'a', 1, 'a', NULL);"
        nb = self.wrapper.query(query)
        assert (nb == 1), f"The number of insertions should be 1, but current output is {nb}."

    def test_update(self):
        query = "UPDATE Genes SET start = 'b' WHERE gene_id = 1;"
        nb = self.wrapper.query(query)
        assert (nb == 1), f"The number of updates should be 1, but current output is {nb}."

    def test_delete(self):
        query = "DELETE FROM Genes WHERE gene_id = 2;"
        nb = self.wrapper.query(query)
        assert (nb == 1), f"The number of deletes should be 1, but current output is {nb}."


if __name__ == '__main__':
    unittest.main(verbosity=2)  # Run all tests with verbose mode
