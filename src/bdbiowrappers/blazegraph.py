#!/usr/bin/env python3
# encoding: utf-8
# =============================================================================
# Class for interacting with Blazegraph (RDF triplestore)
# =============================================================================

from SPARQLWrapper import SPARQLWrapper, JSON, POST
import logging

logging.basicConfig(format='[%(levelname)s] - %(name)s - %(asctime)s : %(message)s')
logger = logging.getLogger("postgresql")
logger.setLevel(logging.DEBUG)


class WrapperBlazegraph:
    """
    WrapperBlazegraph class for parsing and querying RDF data stored in BlazeGraph.

    Results of SPARQL queries are encapsulated in a dictionary. Exemple of result for counting all triples:
    {'head': {'vars': ['nb']}, 'results': {'bindings':
        [{'nb': {'datatype': 'http://www.w3.org/2001/XMLSchema#integer', 'type': 'literal', 'value': '137992'}}]
    }}

    Uses the sparqlwrapper library:
    https://sparqlwrapper.readthedocs.io/
    """

    def __init__(self, server, namespace):
        self.server = server  # server should have a trailing slash
        self.namespace = namespace
        self.conn = SPARQLWrapper(self.server + self.namespace)
        self.conn.setReturnFormat(JSON)

    def query(self, query):
        self.conn.setReturnFormat(JSON)
        self.conn.setQuery(query)
        try:
            res = self.conn.queryAndConvert()
        except Exception as e:
            res = None
            logger.exception(e)
        return res

    def update(self, query):
        # used for queries INSERT DATA and DELETE DATA (but no real update command)
        self.conn.setMethod(POST)
        self.conn.setQuery(query)
        try:
            res = self.conn.query()
        except Exception as e:
            res = None
            logger.exception(e)
        return res

    # open and load triples contained in a file
    def load_file(self, file_n3):
        fd = open(file_n3, 'r')
        lines = fd.read()
        fd.close()
        return self.update('INSERT DATA {' + lines + '}')

