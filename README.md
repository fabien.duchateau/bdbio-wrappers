# Adaptateurs Python - projet BDBIO

Ces adaptateurs ("wrappers") permettent d'interroger différents modèles de données: base de données relationnelle (SQLite ou PostgreSQL), base de données MongoDB, base de données BlazeGraph, fichier XML, fichier JSON, et fichier RDF.

Les adaptateurs sont dans le répertoire `src/bdbiowrappers`. Chaque adaptateur fournit une classe à instancier. Des tests unitaires sont disponibles dans le répertoire `src/bdbiowrappers/tests` et font office d'exemples d'utilisation de ces adaptateurs.

_Attention : il est souvent plus rapide et plus facile de vérifier les résultats en testant vos requêtes dans un SGBD (PostgreSQL, SQLite, BaseX, MongoDB, BlazeGraph ou GraphDB) que directement via ces adaptateurs._

## Installation

Créez et utilisez un environnement virtuel :
```
python -m venv .venv
source .venv/bin/activate
```

Si besoin, mettez à jour la version de pip :
```
pip install --upgrade pip
```

Dans un terminal, utilisez pip :
```
python3 -m pip install git+https://fduchate@forge.univ-lyon1.fr/fabien.duchateau/bdbio-wrappers.git
```

Copiez le fichier `config.yaml` dans votre répertoire (avec votre code Python) et **complétez-le avec les informations d'authentification** aux serveurs PostgreSQL, MongoDB et Blazegraph.
```
postgres_host: bd-pedago.univ-lyon1.fr
postgres_user:  # TODO
postgres_password:  # TODO
postgres_db:  # TODO

mongo_host: mongodb://bd-pedago.univ-lyon1.fr:27017/
mongo_user:  # TODO
mongo_password:  # TODO
mongo_db:  # TODO
mongo_collection:  # TODO

blazegraph_host: http://192.168.77.137:8080/blazegraph/namespace/
blazegraph_namespace:  # TODO
```

Dans votre code, utilisez les wrappers en vvous inspirant des exemples suivants.

## Adaptateur *postgresql.py*

La classe `WrapperPostgres` permet de se connecter à une BD PostgreSQL et de l'interroger via des requêtes SQL.

Voir aussi la [documentation de la librairie psycopg](https://www.psycopg.org/docs/).

Exemple de code :

```python
from bdbiowrappers.postgresql import WrapperPostgres
from bdbiowrappers.utils import load_config_file

config = load_config_file()  # charge les config des BD (par défaut config.yaml)

wrapper = WrapperPostgres(host=config['postgres_host'], username=config['postgres_user'], password=config['postgres_password'], db=config['postgres_db'])

# create tables and instances from a script
wrapper.load_file('bd.postgresql')

 # executes a select SQL query
query = 'SELECT * FROM Genes'
res = wrapper.query(query)
for row in res :  # résultats sous forme de liste de tuples
    print(row)
    
 # executes an insert SQL query
query = "INSERT INTO Genes VALUES (9, 'a', 'a', 'a', 'a', 1, 'a', NULL);"
nb = wrapper.query(query)  # nb contient le nombre de tuples affectés par la requête
```

## Adaptateur *mongo.py*

La classe `WrapperMongo` permet de se connecter à une BD MongoDB et de l'interroger via des requêtes standard.

Voir aussi la [documentation de la librairie pymongo](http://api.mongodb.com/python/current/).

Exemple de code :

```python
from bdbiowrappers.mongo import WrapperMongo
from bdbiowrappers.utils import load_config_file

config = load_config_file()  # charge les config des BD (par défaut config.yaml)

wrapper = WrapperMongo(host=config['mongo_host'], username=config['mongo_user'],
                       password=config['mongo_password'], db=config['mongo_db'],
                       collection=config['mongo_collection'])

# vide la collection
wrapper.drop_collection()

# insertion d'une liste de documents
documents = [{'a': 'hello', 'b': 1}, {'a': 'world', 'b': 2}]
res = wrapper.insert(documents)  # res contient des informations sur l'insertion

# retrouve les documents avec un b supérieur à 1, avec le champ a dans les résultats
res = wrapper.query({'b': {'$gt': 1}}, ['a']) 
print(list(res)[0])  # affiche le premier résultat

 # récupère un document au hasard
doc = wrapper.get_one_document()
print(doc)
```

## Adaptateur *blazegraph.py*

La classe `WrapperBlazegraph` permet de se connecter à un namespace (BD) BlazeGraph et de l'interroger via des requêtes SPARQL.

Voir aussi la [documentation de la librairie sparqlwrapper](https://sparqlwrapper.readthedocs.io/).

Exemple de code :

```python
from bdbiowrappers.blazegraph import WrapperBlazegraph
from bdbiowrappers.utils import load_config_file

config = load_config_file()  # charge les config des BD (par défaut config.yaml)

wrapper = WrapperBlazegraph(server=config['blazegraph_host'], namespace=config['blazegraph_namespace'])

# suppression de tous les triplets
wrapper.update('CLEAR ALL')

# suppression d'un triplet
wrapper.update('''
               DELETE DATA { <http://example/s1> <http://example/p1> <http://example/o1> . }
               ''')

# exécution d'un fichier de triplets
wrapper.load_file('bd.ttl')

# éxécution d'une requête SPARQL
res = wrapper.query("""SELECT ?p ?o WHERE
                       { <http://example/s1> ?p ?o . }
                    """)
for r in res['results']['bindings']:  # tableau des résultats
    print(r['p']['value']) # p corresponds to the SPARQL variable ?p
```

## Adaptateur *xml_file.py*

La classe `WrapperXML` permet de lire un fichier XML et de l'interroger via des requêtes XPath.

Voir aussi la [documentation de la librairie etree](https://docs.python.org/3/library/xml.etree.elementtree.html#module-xml.etree.ElementTree).

Exemple de code :

```python
from bdbiowrappers.xml_file import WrapperXML

wrapper = WrapperXML('bd.xml')

racine = wrapper.get_root()
print(racine.tag)  # affiche la balise de la racine

# exécution d'une requête XPath
entries = wrapper.query(".//{http://uniprot.org/uniprot}entry")
for entry in entries:
    print(entry)
```

## Adaptateur *sqlite.py*

La classe `WrapperSQLite` permet de se connecter à une BD SQLite et de l'interroger via des requêtes SQL.

Voir aussi la [documentation de la librairie sqlite](https://docs.python.org/3/library/sqlite3.html).

Exemple de code :

```python
from bdbiowrappers.sqlite import WrapperSQLite

wrapper = WrapperSQLite('base.db')

# exécution d'un script SQL contenu dans un fichier
wrapper.load_file('bd.sqlite')

# exécution d'une requête SQL
query = 'SELECT * FROM Genes'
res = wrapper.query(query)
for row in res :
    print(row)
```

## Adaptateur *json_file.py*

La classe `WrapperJSON` permet de lire un fichier JSON et de le convertir en un dictionnaire Python.

Voir aussi la [documentation de la librairie json](https://docs.python.org/3/library/json.html?highlight=json).

Exemple de code :

```python
from bdbiowrappers.json_file import WrapperJSON

wrapper = WrapperJSON('documents.json')

# conversion du fichier JSON en dictionnaire Python
dict_json = wrapper.parse_to_dict()
print(dict_json)
```

## Adaptateur *rdf_file*

La classe `WrapperRDF` permet de lire un fichier RDF (séralisations Turtle, N3, etc.) et de l'interroger via des requêtes SPARQL.

Voir aussi la [documentation de la librairie rdflib](https://rdflib.readthedocs.io/en/latest/).

Exemple de code :

```python
from bdbiowrappers.rdf_file import WrapperRDF

wrapper = WrapperRDF('bd.ttl', 'n3')

# comptage du nombre de triplets
nb_triples = wrapper.count()
print(nb_triples)

# exécution d'une requête SPARQL
query = """SELECT DISTINCT ?o 
           WHERE  { <http://example/s1> ?p ?o . }
        """
res = wrapper.query(query)
for row in res :
    print(row)
```

## Autres parsers (*csv*, *tab*, *pubmed*)

Il existe déjà des packages sur [pypi](https://pypi.org/):

- le package [csv](https://docs.python.org/3/library/csv.html) pour les formats CSV, TSV et TAB
- le package [nbib](https://github.com/holub008/nbib) ou [pubmed-lookup](https://github.com/mfcovington/pubmed-lookup) pour le format PUBMED


## Tests unitaires

Les tests ne sont pas packagés, mais [disponibles sur le dépôt](/src/bdbiowrappers/tests/).

Pour lancer les tests unitaires (cloner le dépôt ssi besoin) :
```bash
# attention: les paramètres d'accès aux serveurs sont à modifier dans config.yaml (et le copier dans tests/)
# pour lancer le test d'un wrapper
cd src/bdbiowrappers/tests/
python3 test_postgresql.py
python3 test_mongo.py
python3 test_blazegraph.py
python3 test_sqlite.py
python3 test_xml.py
python3 test_rdf.py
python3 test_json.py
# pour lancer tous les tests
cd src/bdbiowrappers/tests/
python3 -m unittest -v
```


